import type { MouseEventHandler } from "react";
import { ChannelList, ChannelListProps } from "stream-chat-react";

import { MessagingChannelListHeader, MessagingChannelPreview } from "../index";
import { useThemeContext } from "../../context";
import logoutIcon from "../../assets/logout.png";
import Cookies from "universal-cookie";

const cookies = new Cookies();
type MessagingSidebarProps = {
  channelListOptions: {
    filters: ChannelListProps["filters"];
    sort: ChannelListProps["sort"];
    options: ChannelListProps["options"];
  };
  onClick: MouseEventHandler;
  onCreateChannel: () => void;
  onPreviewSelect: MouseEventHandler;
};

const MessagingSidebar = ({
  channelListOptions,
  onClick,
  onCreateChannel,
  onPreviewSelect,
}: MessagingSidebarProps) => {
  const { themeClassName } = useThemeContext();

  const logout = () => {
    cookies.remove("token");
    cookies.remove("userId");
    cookies.remove("username");
    cookies.remove("fullName");
    cookies.remove("hashedPassword");

    window.location.reload();
  };
  return (
    <div
      className={`str-chat messaging__sidebar ${themeClassName}`}
      id="mobile-channel-list"
      onClick={onClick}
    >
      <MessagingChannelListHeader onCreateChannel={onCreateChannel} />
      <ChannelList
        {...channelListOptions}
        Preview={(props) => (
          <MessagingChannelPreview {...props} onClick={onPreviewSelect} />
        )}
      />

      <div className="logout-button" onClick={logout}>
        <img src={logoutIcon} alt="" width={20} height={20} />
        <p>Logout</p>
      </div>
    </div>
  );
};

export default MessagingSidebar;
