import React from "react";
import ReactDOM from "react-dom/client";
import * as Sentry from "@sentry/react";
import "./index.css";
import App from "./App";
import { ThemeContextProvider } from "./context";
import { Toaster } from "react-hot-toast";

if (process.env.NODE_ENV === "production") {
  Sentry.init({
    dsn: "https://cfe9b30508b44e40908da83aee0743e9@o389650.ingest.sentry.io/5556314",
    integrations: [new Sentry.BrowserTracing()],
    tracesSampleRate: 1.0,
  });
}

const urlParams = new URLSearchParams(window.location.search);

const targetOrigin =
  urlParams.get("target_origin") ||
  (process.env.REACT_APP_TARGET_ORIGIN as string);

const container = document.getElementById("root");
const root = ReactDOM.createRoot(container!);
root.render(
  <React.StrictMode>
    <ThemeContextProvider targetOrigin={targetOrigin}>
      <App />
      <Toaster />
    </ThemeContextProvider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
