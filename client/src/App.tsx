import { StreamChat } from "stream-chat";
import { getImage } from "./assets";
import { getChannelListOptions } from "./channelListOptions";
import Home from "./pages/home";
import Cookies from "universal-cookie";
import Auth from "./components/Auth";
const cookies = new Cookies();

const urlParams = new URLSearchParams(window.location.search);
const apiKey =
  urlParams.get("api_key") || (process.env.REACT_APP_STREAM_KEY as string);

const authToken = cookies.get("token");
const userId = cookies.get("userId");
const name = cookies.get("name");
const fullName = cookies.get("fullName");
const hashedPassword = cookies.get("hashedPassword");
const targetOrigin =
  urlParams.get("target_origin") ||
  (process.env.REACT_APP_TARGET_ORIGIN as string);

const noChannelNameFilter = urlParams.get("no_channel_name_filter") || true;
// const skipNameImageSet = urlParams.get("skip_name_image_set") || false;

const channelListOptions = getChannelListOptions(!!noChannelNameFilter, userId);
const userToConnect: {
  id: string;
  name?: string;
  fullName?: string;
  hashedPassword?: string;
} = {
  id: userId,
  name,
  fullName,
  hashedPassword,
};

const App = () => {
  if (!authToken) return <Auth />;
  return (
    <Home
      apiKey={apiKey!}
      userToConnect={userToConnect}
      userToken={authToken}
      targetOrigin={targetOrigin!}
      channelListOptions={channelListOptions}
    />
  );
};

export default App;
